const net = require("net");
var clients = [];
net.createServer(function(socket) {
    socket.write("Hello there! Please enter your name: ");
    socket.on("data", function(data) {
        const cleanedUp = data.toString().trim();
        if(!socket.name) {
            socket.name = cleanedUp;
            clients.push(socket);
            console.log(socket.name + " joined the conversation.");
            clients.forEach(function(val) {
                if(val !== socket) {
                    val.write(socket.name + " joined the conversation.");
                }
            });
        }
        else {
            console.log(socket.name + " said: " + cleanedUp);
            clients.forEach(function(val) {
                if(val !== socket) {
                    val.write(socket.name + " said: " + cleanedUp + "\n");
                }
            });
        }
    });
    socket.on("close", function() {
        console.log(socket.name + " left the conversation.\n");
        for(var i = 0; i < clients.length; i++) {
            if(clients[i] == socket) {
                clients.splice(i, 1);
            }
        }
        clients.forEach(function(val) {
            if(val !== socket) {
                val.write(socket.name + " left the conversation.\n");
            }
        });
    });
}).listen(3030, "localhost");